﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Draw2D : MonoBehaviour {

    [Range(0,0.5f)]
    public float brushSize = .05f;

    public float scale = .2f;

    public Texture2D sourceTex; //用于擦除的图片
    private Texture2D maskTex;  //遮罩图像

    public RawImage rawIamge;

    private bool isCreate = false;

    private void Start()
    {
        rawIamge.texture = sourceTex;
    }

    void Update () {

        if (Input.GetMouseButton(0))
            Erase();

        if (Input.GetKeyDown(KeyCode.R))
            Reset();

    }

    private void Reset()
    {
        FillEmptyMask();
        isCreate = false;
    }


    public Canvas canvas;
    private void Erase()
    {
        Vector2 uv = Vector2.zero;

        Vector2 postion;
        //警告：这里的camera 必须填canvas.worldCamera，否则局部坐标会出错误
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rawIamge.rectTransform, Input.mousePosition, canvas.worldCamera, out postion))
        {
            //Debug.Log(postion);

            if (!RectTransformUtility.RectangleContainsScreenPoint(rawIamge.rectTransform, Input.mousePosition)) return;

            float width = rawIamge.rectTransform.sizeDelta.x;
            float height = rawIamge.rectTransform.sizeDelta.y;
            float u = (postion.x + width / 2) / width;
            float v = (postion.y + height / 2) / height;

            uv = new Vector2(u, v);
        }

        if (!isCreate)
        {
            isCreate = true;

            FillEmptyMask();
        }
        else
        {
            FillMaskDraw(uv);
        }

    }

    private void FillMaskDraw(Vector2 uv)
    {
        int pixelX = (int)(uv.x * maskTex.width);
        int pixelY = (int)(uv.y * maskTex.height);

        int luPosX = Mathf.Max(0, (int)(pixelX - brushSize * .5f * maskTex.width));
        int luPosY = Mathf.Max(0, (int)(pixelY - brushSize * .5f * maskTex.height));
        int rdPosX = Mathf.Min(maskTex.width, (int)(pixelX + brushSize * .5f * maskTex.width));
        int rdPosY = Mathf.Min(maskTex.height, (int)(pixelY + brushSize * .5f * maskTex.height));

        int ww = Mathf.Abs(rdPosX - luPosX);
        int hh = Mathf.Abs(rdPosY - luPosY);

        Color[] cols = new Color[ww * hh];
        for (int i = 0; i < cols.Length; i++)
        {
            cols[i].a = 0;
        }

        maskTex.SetPixels(luPosX, luPosY, ww, hh, cols);
        maskTex.Apply();

        rawIamge.material.SetTexture("_MaskTex", maskTex);
    }

    private void FillEmptyMask()
    {
        

        int width = (int)(sourceTex.width * scale);
        int height = (int)(sourceTex.height * scale);

        maskTex = new Texture2D(width, height);
        Color32[] cols = new Color32[width * height];
        for (int i = 0; i < cols.Length; i++)
        {
            cols[i] = Color.white;
        }


        maskTex.SetPixels32(cols);
        maskTex.Apply();

        rawIamge.material.SetTexture("_MaskTex", maskTex);
    }
}
