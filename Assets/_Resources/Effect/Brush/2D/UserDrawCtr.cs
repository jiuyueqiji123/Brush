﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UserDrawCtr : HandDrawBase{

    #region ui
    public RawImage _FillTexture_draw;
    public RectTransform _Pen;
    public RawImage _BrushTex;
    private Vector2 _targetTexSize;

    #endregion

    private Texture2D m_DrawingTex_Paint;
    private Texture2D m_DrawingTex_MainTex_Paint;
    private Color32[] m_DrawingPiexls_paint;
    private Color32[] m_DrawingPixels_MainTex_paint;

    private float m_pathlength_calc_timer;
    private bool m_istimer_start;
    private List<Vector3> userdraw_way_points_record;
    private float m_current_guidepath_length;

    public Canvas _canvas;

    protected override void Init()
    {
        _targetTexSize = _FillTexture_draw.rectTransform.sizeDelta;
        userdraw_way_points_record = new List<Vector3>();
        StartDraw();
    }

    private void OnDestroy()
    {

    }

    public void StartDraw(int mask_size = 1, Action complete_callback = null)
    {
        //玩家手绘的准备
        this._mask_size = mask_size;

        //右侧画板
        //前景白板
        m_DrawingTex_MainTex_Paint = new Texture2D((int)_targetTexSize.x, (int)_targetTexSize.y, TextureFormat.ARGB32, false, false);
        m_DrawingTex_MainTex_Paint.filterMode = FilterMode.Point;
        m_DrawingPixels_MainTex_paint = m_DrawingTex_MainTex_Paint.GetPixels32();
        int count = m_DrawingPixels_MainTex_paint.Length;
        for (int i = 0; i < count; i++)
        {
            m_DrawingPixels_MainTex_paint[i] = Color.white;
        }
        m_DrawingTex_MainTex_Paint.SetPixels32(m_DrawingPixels_MainTex_paint);
        m_DrawingTex_MainTex_Paint.Apply(false);

        //黑色蒙版
        m_DrawingTex_Paint = new Texture2D((int)_targetTexSize.x * mask_size, (int)_targetTexSize.y * mask_size, TextureFormat.ARGB32, false, false);
        m_DrawingTex_Paint.filterMode = FilterMode.Point;
        m_DrawingPiexls_paint = m_DrawingTex_Paint.GetPixels32();
        count = m_DrawingPiexls_paint.Length;
        for (int i = 0; i < count; i++)
        {
            m_DrawingPiexls_paint[i] = Color.black;
        }
        m_DrawingTex_Paint.SetPixels32(m_DrawingPiexls_paint);
        m_DrawingTex_Paint.Apply(false);

        m_mask_tex_width = (int)_targetTexSize.x * mask_size;
        m_mask_tex_height = (int)_targetTexSize.y * mask_size;


        //右侧画板
        _FillTexture_draw.material.SetTexture("_MaskTex", m_DrawingTex_Paint);
        _FillTexture_draw.material.SetTexture("_MainTex", m_DrawingTex_MainTex_Paint);
        _FillTexture_draw.SetNativeSize();

        //设置右侧笔刷
        m_BrushWidth = _BrushTex.texture.width;
        m_BrushHeight = _BrushTex.texture.height;
        m_BrushHalfWidth = (int)(m_BrushWidth / 2);
        m_BrushHalfHeight = (int)(m_BrushHeight / 2);
        m_BrushPiexls = (_BrushTex.texture as Texture2D).GetPixels32();
        m_CurColor = Color.red;
    }

    public void ClearDrawPixel()
    {
        int count = m_DrawingPiexls_paint.Length;
        for (int i = 0; i < count; i++)
        {
            m_DrawingPiexls_paint[i] = Color.black;
        }
        m_DrawingTex_Paint.SetPixels32(m_DrawingPiexls_paint);
        m_DrawingTex_Paint.Apply(false);
    }

    void Update()
    {
        if (_IsActive)
        {

            if (Input.GetMouseButton(0))
            {
                if (!m_StartDrawing_Paint)
                {
                    TouchBegin(Input.mousePosition);
                }
                else
                {
                    TouchMove(Input.mousePosition);
                }
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            TouchEnd();
        }
    }

    public void TouchBegin(Vector3 inputScreenPosition)
    {
        Vector2 rect_pos;
        GetRectPosForScreenPos(_canvas, inputScreenPosition, out rect_pos);
        _Pen.anchoredPosition = rect_pos;

        TouchBegin(_Pen);
    }

    public void TouchBegin(RectTransform mov_obj)
    {
        m_StartDrawing_Paint = true;

        int u, v;
        if (GetInputUV(mov_obj.anchoredPosition, _FillTexture_draw, out u, out v))
        {
            m_CurUV_Paint = new Vector2(u, v);
            m_OldUV_Paint = m_CurUV_Paint;
        }
    }

    public void TouchEnd()
    {
        if (m_StartDrawing_Paint)
        {
            m_StartDrawing_Paint = false;
        }
    }

    public void TouchMove(Vector3 inputScreenPosition)
    {
        Vector2 rect_pos;
        GetRectPosForScreenPos(_canvas, inputScreenPosition, out rect_pos);
        _Pen.anchoredPosition = rect_pos;

        TouchMove(_Pen);
    }

    public void TouchMove(RectTransform mov_obj)
    {
        int u = 0;
        int v = 0;
        if (GetInputUV(mov_obj.anchoredPosition, _FillTexture_draw, out u, out v))
        {
            m_OldUV_Paint = m_CurUV_Paint;
            m_CurUV_Paint = new Vector2(u, v);
            if (m_OldUV_Paint == Vector2.zero)
            {
                m_OldUV_Paint = m_CurUV_Paint;
            }

            //Debug.LogError("TouchPhase.Moved_m_OldUV:" + m_OldUV + "m_CurUV" + m_CurUV);
            DrawBrush((int)m_CurUV_Paint.x, (int)m_CurUV_Paint.y, m_DrawingPiexls_paint);

            //Debug.LogError("m_OldUV:" + m_OldUV + "m_CurUV" + m_CurUV);
            if (Vector2.Distance(m_OldUV_Paint, m_CurUV_Paint) > (m_BrushWidth * 0.33f))
            {
                //Debug.LogError("DrawLineWithBrush:");
                DrawLineWithBrush(m_OldUV_Paint, m_CurUV_Paint, m_DrawingPiexls_paint);
            }
            m_DrawingTex_Paint.SetPixels32(0, 0, m_mask_tex_width, m_mask_tex_height, m_DrawingPiexls_paint, 0);
            m_DrawingTex_Paint.Apply(false);
        }
    }

    public static bool GetRectPosForScreenPos(Canvas cvs, Vector2 ScreenPos, out Vector2 RectTransPos)
    {
        return RectTransformUtility.ScreenPointToLocalPointInRectangle(cvs.transform as RectTransform, ScreenPos, cvs.worldCamera, out RectTransPos);
    }
}
