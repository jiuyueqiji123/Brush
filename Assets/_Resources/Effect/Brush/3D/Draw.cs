﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draw : MonoBehaviour {

    [Range(0,0.5f)]
    public float brushSize = .05f;

    public float scale = .2f;

    private Texture2D hitTex;
    private Texture2D maskTex;

    public MeshRenderer render;

    public LayerMask layer;

    private bool isCreate = false;


	void Start () {
		
	}
	
	void Update () {

        if (Input.GetMouseButton(0))
            Erase();

        if (Input.GetKeyDown(KeyCode.R))
            Reset();

    }

    private void Reset()
    {
        FillEmptyMask();
        isCreate = false;
    }

    private void Erase()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layer))
        {
            //警告：这里只对meshCollider起作用，否则永远返回vector2.zero;
            Vector2 uv = hit.textureCoord;
            //Debug.Log(uv);

            if (!isCreate)
            {
                isCreate = true;

                hitTex = (Texture2D)hit.transform.GetComponent<MeshRenderer>().sharedMaterial.GetTexture("_ForeGroundTex");

                FillEmptyMask();
            }
            else
            {
                FillMaskDraw(uv);
            }

        }

    }

    private void FillMaskDraw(Vector2 uv)
    {
        int pixelX = (int)(uv.x * maskTex.width);
        int pixelY = (int)(uv.y * maskTex.height);

        int luPosX = Mathf.Max(0, (int)(pixelX - brushSize * .5f * maskTex.width));
        int luPosY = Mathf.Max(0, (int)(pixelY - brushSize * .5f * maskTex.height));
        int rdPosX = Mathf.Min(maskTex.width, (int)(pixelX + brushSize * .5f * maskTex.width));
        int rdPosY = Mathf.Min(maskTex.height, (int)(pixelY + brushSize * .5f * maskTex.height));

        int ww = Mathf.Abs(rdPosX - luPosX);
        int hh = Mathf.Abs(rdPosY - luPosY);

        Color[] cols = new Color[ww * hh];
        for (int i = 0; i < cols.Length; i++)
        {
            cols[i].a = 0;
        }

        maskTex.SetPixels(luPosX, luPosY, ww, hh, cols);
        maskTex.Apply();

        render.material.SetTexture("_MaskTex", maskTex);
    }

    private void FillEmptyMask()
    {
        

        int width = (int)(hitTex.width * scale);
        int height = (int)(hitTex.height * scale);

        maskTex = new Texture2D(width, height);
        Color32[] cols = new Color32[width * height];
        for (int i = 0; i < cols.Length; i++)
        {
            cols[i] = Color.white;
        }


        maskTex.SetPixels32(cols);
        maskTex.Apply();

        render.material.SetTexture("_MaskTex", maskTex);
    }
}
