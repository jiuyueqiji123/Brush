﻿Shader "Unlit/Masker"
{
	Properties
	{
		_Alpha("Alpha",Range(0,1)) = 0.5
		_ForeGroundTex("ForeGroundTex", 2D) = "white" {}
		_MainTex ("MainTexture", 2D) = "white" {}
		_MaskTex("MaskTexture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			float _Alpha;
			sampler2D _MaskTex;
			sampler2D _ForeGroundTex;
			sampler2D _MainTex;
			float4 _MainTex_ST;

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 mcol = tex2D(_MaskTex, i.uv);
				fixed4 fcol = tex2D(_ForeGroundTex, i.uv);
				fcol.a = mcol.a * _Alpha;
				//fcol.rgb *= mcol.a * _Alpha;

				//fixed4 col = tex2D(_MainTex, i.uv);
				//col.a = (1 - mcol.a) * _Alpha;
				//col.rgb *= (1 - mcol.a) * _Alpha;

				//fixed4 finalCol = fcol + col;

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, finalCol);
				return fcol;
			}
			ENDCG
		}
	}
}
