﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/DrawingShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_MaskTex ("MaskTextrue",2D) = "white" {}
		_Alpha ("Alpha",Range(0,1)) = 0
	}
	SubShader
	{
		Tags {"Queue"="Transparent+500" "RenderType"="Transparent" }
		//ZWrite On
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha 
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _MaskTex;
			float4 _MainTex_ST;
			float _Alpha;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 mask = tex2D(_MaskTex, i.uv);

				//fixed4 tmpvar_2;
				//tmpvar_2 = ddx(col);
				//fixed4 tmpvar_3;
				//tmpvar_3 = ddy(col);
				//fixed a = sqrt((tmpvar_2 * tmpvar_2) + (tmpvar_3 * tmpvar_3));//tmpvar_2.a + tmpvar_3;//clamp(sqrt((tmpvar_2 * tmpvar_2) + (tmpvar_3 * tmpvar_3)),0,1);
				//col = fixed4(1,1,1,a);
				//mask.a = clamp ((col / (2.0 * sqrt(((tmpvar_2 * tmpvar_2) + (tmpvar_3 * tmpvar_3))))), 0.0, 0.6).a;
				   
				//mix( vec3(1.0,0.6,0.0), col, smoothstep(-w,w,d-0.04));
				col = mask * mask.a + col * (1 - mask.a);
				col.a *= _Alpha;
				//col = lerp(col,mask,mask.a * (1 - _Alpha));
				//col = lerp(mask,col,col.a);
				return col;
			}
			ENDCG
		}
	}
}
