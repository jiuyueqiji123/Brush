﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract  class HandDrawBase : MonoBehaviour {

    public bool _IsActive;


    protected Color32[] m_BrushPiexls;
    [HideInInspector]
    public Texture2D m_TargetTex;
    protected int m_mask_tex_width;   //被填色的图片的宽度
    protected int m_mask_tex_height;//被填色的图片的高度
    protected int m_BrushWidth;       //
    protected int m_BrushHeight;      //
    protected int m_BrushHalfWidth;   //刷子图片半宽
    protected int m_BrushHalfHeight;  //刷子图片半高

    protected Vector2 m_CurUV_Paint;
    protected Vector2 m_OldUV_Paint;
    protected Vector2 m_filltex_draw_originPoint;
    protected Vector3 m_dragOffset;
    protected Vector3 m_prevousBrushPoint_Paint;
    protected bool m_StartDrawing_Paint;
    protected Color32 m_CurColor;
    protected bool m_EraserModel;
    protected int _mask_size;

    private void Awake()
    {
        Init();
    }

    protected abstract void Init();

    //获取touch的uv坐标
    public bool GetInputUV(Vector2 brushAnchorPos, RawImage fill_texture, out int x, out int y)
    {
        Vector2 filltex_sizedelta = fill_texture.rectTransform.sizeDelta;
        Vector2 originAnchorPoint = fill_texture.rectTransform.anchoredPosition - new Vector2(filltex_sizedelta.x, filltex_sizedelta.y) / 2;
        Vector2 dir = brushAnchorPos - originAnchorPoint;
        x = (int)dir.x * _mask_size;
        y = (int)dir.y * _mask_size;
        //Debug.Log("x: " + x + "y: " + y + "sizedelta: " + filltex_sizedelta);

        if (x < 0 || y < 0 || x > filltex_sizedelta.x * _mask_size || y > filltex_sizedelta.y * _mask_size)
            return false;

        return true;
    }

    protected virtual void DrawBrush(int px, int py,Color32[] m_DrawingPiexls_paint)
    {
        //Debug.LogError("DrawBrush:" + px + "___" + py);
        int stX = Mathf.Clamp(px - m_BrushHalfWidth, 0, m_mask_tex_width);
        int stY = Mathf.Clamp(py - m_BrushHalfHeight, 0, m_mask_tex_height);
        int endX = Mathf.Clamp(px + m_BrushHalfWidth, 0, m_mask_tex_width);
        int endY = Mathf.Clamp(py + m_BrushHalfHeight, 0, m_mask_tex_height);

        int lengthX = endX - stX;
        int lengthY = endY - stY;

        int max = m_mask_tex_width * m_mask_tex_height - 1 - m_mask_tex_width;
        int pixel = m_mask_tex_width * stY + stX;   //当前刷子起始像素在目标图片的位置
        //Debug.LogError("pixel:" + pixel);
        int BrushPiexl;

        for (int y = 0; y < lengthY; y++)
        {
            for (int x = 0; x < lengthX; x++)
            {
                BrushPiexl = m_BrushWidth * y + x;

                if (m_EraserModel)
                {
                    if (m_BrushPiexls[BrushPiexl].a > 25)
                    {
                        //恢复颜色和透明度
                        m_DrawingPiexls_paint[pixel] = Color32.Lerp(m_DrawingPiexls_paint[pixel], Color.black, m_BrushPiexls[BrushPiexl].a / 255f); ;
                    }
                }
                else
                {
                    if (m_BrushPiexls[BrushPiexl].a > 250)
                    {
                        m_DrawingPiexls_paint[pixel] = m_CurColor;
                    }
                    else
                    {
                        m_DrawingPiexls_paint[pixel] = Color32.Lerp(m_DrawingPiexls_paint[pixel], m_CurColor, m_BrushPiexls[BrushPiexl].a / 255f);
                    }

                }

                pixel++;
            }
            pixel = Mathf.Clamp((m_mask_tex_width * (stY + y + 1) + stX + 1), 0, max);
        }
    }



    protected void DrawLineWithBrush(Vector2 start, Vector2 end, Color32[] m_DrawingPiexls_paint)
    {
        int x0 = (int)start.x;
        int y0 = (int)start.y;
        int x1 = (int)end.x;
        int y1 = (int)end.y;
        int dx = Mathf.Abs(x1 - x0); // TODO: try these? http://stackoverflow.com/questions/6114099/fast-integer-abs-function
        int dy = Mathf.Abs(y1 - y0);
        int sx, sy;
        if (x0 < x1) { sx = 1; } else { sx = -1; }
        if (y0 < y1) { sy = 1; } else { sy = -1; }
        int err = dx - dy;
        bool loop = true;
        //			int minDistance=brushSize-1;
        int minDistance = (int)(m_BrushWidth / 4);
        int pixelCount = 0;
        int e2;
        while (loop)
        {
            pixelCount++;
            if (pixelCount > minDistance)
            {
                pixelCount = 0;
                DrawBrush(x0, y0, m_DrawingPiexls_paint);
            }
            if ((x0 == x1) && (y0 == y1)) loop = false;
            e2 = 2 * err;
            if (e2 > -dy)
            {
                err = err - dy;
                x0 = x0 + sx;
            }
            if (e2 < dx)
            {
                err = err + dx;
                y0 = y0 + sy;
            }
        }

    }

}
